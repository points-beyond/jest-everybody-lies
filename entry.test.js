import { save } from "./api";
import Entry from "./entry";

jest.mock("./api");

describe("Entry", () => {
  it("correctly saves instance", async () => {
    const FAKE_FIELDS = { a: 1, b: 2 };
    const entry = new Entry();

    await entry.save(FAKE_FIELDS);

    expect(save).toHaveBeenCalledWith(FAKE_FIELDS);
  });
});
